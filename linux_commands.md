# Linux Commands

## ls - list directory contents
When we have to list the directory contents we use ls command. Some common flags used with ls are:

    ls [flags] [File]

- **-l**: Use a long listing format, showing details such as permissions, owner, group, size, and date modified 
- **-a**: Show hidden files, which are files that start with a dot (.)
- **-h**: Show sizes in human-readable format, for example, 1K, 20K, 80M instead of numbers in bytes
- **-t**: It sort all the files by modification time, the new modification comes first.
- **-r**: Reverse the order of the sort, for example, sort files by modification time, oldest first

## pwd - prints the name of current or working directory

We use pwd command when we want to display the absolute path of the current working directory.
 
## rm - remove files or directories
rm is used to delete files or directories. Be careful when using rm, as there is no trash bin or undo for deleted files.

- **-f**: Force removal, do not prompt for confirmation
- **-r**: Remove directories and their contents recursively

## chmod - change file mode bits

When we have to change the permissions of any files and directories chmod command is used.

Some of the basic option we use with chmod for changing permissions are:

- **u**: User/owner permissions
- **g**: Group permissions
- **o**: Other (world) permissions
- **+**: Add permission
- **-**: Remove permission
- **-R**: Operate on files and directories recursively

we can also use decimal number ranging between 0-7 for each element of the permission.
The permission in decimal format are:
- 4 - read
- 2 - write
- 1 - execute
- 6 - read and write
- 5 - read and execute
- 3 - write and execute
- 0 - no permission

## chown - change owner of the file and group
chown is used to change the owner and/or group of a file.

if we want to change the user ownership of given file/directory (root access needed)
```
chown username filename/directory
```
change both user and group:
```
chown username:groupname filename/directory
```
- -R: Operate on files and directories recursively
